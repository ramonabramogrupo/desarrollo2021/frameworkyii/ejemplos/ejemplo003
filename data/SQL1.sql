﻿DROP DATABASE IF EXISTS ejemplo3yii;
CREATE DATABASE ejemplo3yii;
USE ejemplo3yii;

CREATE TABLE alumnos(
  id int,
  nombre varchar(50),
  apellidos varchar(100),
  edad int,
  poblacion varchar(50),
  telefono varchar(12),
  PRIMARY KEY (id)
  );